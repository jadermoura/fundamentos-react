import React from 'react';

class App extends React.Component {

    state = {
        nome: 'Jader Moura'
    }

    render() {
        return (
            <h1>olá {this.state.nome} </h1>
        )
    }
}

export default App;